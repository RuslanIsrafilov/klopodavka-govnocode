package klopodavkaclient;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.util.List;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class GameField extends JPanel {

    int fieldSize;
    int cellsCntW;
    int cellsCntH;
    final Cell[] cells;
    Image bBugPic;
    Image rBugPic;
    Image kbBugPic;
    Image krBugPic;
    int myBaseIndex;
    int oppBaseIndex;
    int movesCnt;
    int curPlayerIndex;

    GameField(int newFieldSize, int newCellsCntW, int newCellsCntH) {
        fieldSize = newFieldSize;
        cellsCntW = newCellsCntW;
        cellsCntH = newCellsCntH;
        cells = new Cell[cellsCntH * cellsCntW];
    }

    ;

    Cell[] getCells() {
        return cells;
    }

    ;

    int getFieldSize() {
        return fieldSize;
    }

    ;

    int getCellsCntW() {
        return cellsCntW;
    }

    ;

    int getCellsCntH() {
        return cellsCntH;
    }

    ;

    void setFieldSize(int newFieldSize) {
        fieldSize = newFieldSize;
    }

    ;

    void setCellsCntW(int newCellCntW) {
        cellsCntW = newCellCntW;
    }

    ;

    void setCellsCntH(int newCellCntH) {
        cellsCntH = newCellCntH;
    }

    ;


    void CreateField(int cellSize, int playerNumber) {

        curPlayerIndex = 1;

        if (Player.getPlayerNumber() == 1) {
            movesCnt = 3;
        }
        if (Player.getPlayerNumber() == 2) {
            movesCnt = 0;
        }
        try {
            bBugPic = ImageIO.read(new File("./2.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            rBugPic = ImageIO.read(new File("./1.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            kbBugPic = ImageIO.read(new File("./2kill.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            krBugPic = ImageIO.read(new File("./1kill.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        setLayout(new GridLayout(cellsCntH, cellsCntW));
        for (int i = 0; i < cellsCntH * cellsCntW; i++) {
            cells[i] = new Cell(i, cellSize, Cell.status.EMPTY);
            cells[i].setPreferredSize(new Dimension(cellSize, cellSize));
            cells[i].setBorder(new LineBorder(Color.BLACK));
            add(cells[i]);
            final int tmpInd = i;
            cells[i].addMouseListener(new java.awt.event.MouseAdapter() {
                public void mousePressed(java.awt.event.MouseEvent evt) {
                    CreateBug(evt, tmpInd, myBaseIndex);
                }
            });
        }

        if (playerNumber == 1) {
            createBlueBase(Cell.status.MY_BASE);
            createRedBase(Cell.status.OPP_BASE);
        }
        if (playerNumber == 2) {
            createBlueBase(Cell.status.OPP_BASE);
            createRedBase(Cell.status.MY_BASE);
        }
        /*addPicToCell(100, bBugPic);
         cells[100].setCurrentStat(Cell.status.MY_BUG);
         addPicToCell(101, rBugPic);
         cells[101].setCurrentStat(Cell.status.OPP_BUG);
         addPicToCell(102, krBugPic);
         cells[102].setCurrentStat(Cell.status.KILLED_BY_ME);
         addPicToCell(103, kbBugPic);
         cells[103].setCurrentStat(Cell.status.KILLED_BY_OPP);*/

    }

    ;

    void createBlueBase(Cell.status status) {
        cells[cellsCntW + 1].setCurrentStat(status);
        cells[cellsCntW + 1].setBackground(Color.BLUE);
        if (status.equals(Cell.status.MY_BASE)) {
            myBaseIndex = cellsCntW + 1;
            oppBaseIndex = cellsCntW * (cellsCntH - 1) - 2;
        }
    }

    ;

    void createRedBase(Cell.status status) {
        cells[cellsCntW * (cellsCntH - 1) - 2].setCurrentStat(status);
        cells[cellsCntW * (cellsCntH - 1) - 2].setBackground(Color.RED);
        if (status.equals(Cell.status.MY_BASE)) {
            myBaseIndex = cellsCntW * (cellsCntH - 1) - 2;
            oppBaseIndex = cellsCntW + 1;
        }
    }

    ;

    boolean CheckArea(int curInd, Cell.status checkedStatus) {
        boolean flag = false;

        //если хочешь поставить жука в первую строку
        if (curInd < cellsCntW) {
            //если в первую клетку первой строки
            if (curInd == 0) {
                if (cells[curInd + 1].getCurrentStat().equals(checkedStatus)
                        || cells[curInd + cellsCntW].getCurrentStat().equals(checkedStatus)
                        || cells[curInd + cellsCntW + 1].getCurrentStat().equals(checkedStatus)) {
                    return true;
                } else {
                    return false;
                }
            }
            //если в последнюю клетку первой строки
            if (curInd == cellsCntW - 1) {
                if (cells[curInd - 1].getCurrentStat().equals(checkedStatus)
                        || cells[curInd + cellsCntW].getCurrentStat().equals(checkedStatus)
                        || cells[curInd + cellsCntW - 1].getCurrentStat().equals(checkedStatus)) {
                    return true;
                } else {
                    return false;
                }
            }
            //остальные клетки первой строки
            if (cells[curInd + 1].getCurrentStat().equals(checkedStatus)
                    || cells[curInd - 1].getCurrentStat().equals(checkedStatus)
                    || cells[curInd + cellsCntW - 1].getCurrentStat().equals(checkedStatus)
                    || cells[curInd + cellsCntW].getCurrentStat().equals(checkedStatus)
                    || cells[curInd + cellsCntW + 1].getCurrentStat().equals(checkedStatus)) {
                return true;
            } else {
                return false;
            }

        }

        //если хочешь поставить жука в последнюю строку
        if (curInd > (cellsCntW * (cellsCntH - 1) - 1)) {
            //если в первую клетку последней строки
            if (curInd == (cellsCntW - 1) * cellsCntH) {
                if (cells[curInd + 1].getCurrentStat().equals(checkedStatus)
                        || cells[curInd - cellsCntW].getCurrentStat().equals(checkedStatus)
                        || cells[curInd - cellsCntW + 1].getCurrentStat().equals(checkedStatus)) {
                    return true;
                } else {
                    return false;
                }
            }
            //если в последнюю клетку последней строки
            if (curInd == cellsCntW * cellsCntH - 1) {
                if (cells[curInd - 1].getCurrentStat().equals(checkedStatus)
                        || cells[curInd - cellsCntW].getCurrentStat().equals(checkedStatus)
                        || cells[curInd - cellsCntW - 1].getCurrentStat().equals(checkedStatus)) {
                    return true;
                } else {
                    return false;
                }
            }
            //остальные клетки последней строки
            if (cells[curInd + 1].getCurrentStat().equals(checkedStatus)
                    || cells[curInd - 1].getCurrentStat().equals(checkedStatus)
                    || cells[curInd - cellsCntW - 1].getCurrentStat().equals(checkedStatus)
                    || cells[curInd - cellsCntW].getCurrentStat().equals(checkedStatus)
                    || cells[curInd - cellsCntW + 1].getCurrentStat().equals(checkedStatus)) {
                return true;
            } else {
                return false;
            }

        }
        //если хочешь поставить клетку в крайний левый столбец
        if (curInd % cellsCntW == 0) {
            if (cells[curInd + 1].getCurrentStat().equals(checkedStatus)
                    || cells[curInd - cellsCntW].getCurrentStat().equals(checkedStatus)
                    || cells[curInd - cellsCntW + 1].getCurrentStat().equals(checkedStatus)
                    || cells[curInd + cellsCntW].getCurrentStat().equals(checkedStatus)
                    || cells[curInd + cellsCntW + 1].getCurrentStat().equals(checkedStatus)) {
                return true;
            } else {
                return false;
            }
        }
        // -//- крайний правый
        if (curInd % cellsCntW == cellsCntW - 1) {
            if (cells[curInd - 1].getCurrentStat().equals(checkedStatus)
                    || cells[curInd - cellsCntW].getCurrentStat().equals(checkedStatus)
                    || cells[curInd - cellsCntW - 1].getCurrentStat().equals(checkedStatus)
                    || cells[curInd + cellsCntW].getCurrentStat().equals(checkedStatus)
                    || cells[curInd + cellsCntW - 1].getCurrentStat().equals(checkedStatus)) {
                return true;
            } else {
                return false;
            }
        }
        // в середину поля
        if (cells[curInd - 1].getCurrentStat().equals(checkedStatus)
                || cells[curInd + 1].getCurrentStat().equals(checkedStatus)
                || cells[curInd - cellsCntW].getCurrentStat().equals(checkedStatus)
                || cells[curInd - cellsCntW - 1].getCurrentStat().equals(checkedStatus)
                || cells[curInd - cellsCntW + 1].getCurrentStat().equals(checkedStatus)
                || cells[curInd + cellsCntW - 1].getCurrentStat().equals(checkedStatus)
                || cells[curInd + cellsCntW].getCurrentStat().equals(checkedStatus)
                || cells[curInd + cellsCntW + 1].getCurrentStat().equals(checkedStatus)) {
            return true;
        } else {
            return false;
        }
    }

    ;

    void addPicToCell(int curInd, Image pic) {
        if (cells[curInd].getComponentCount() == 0) {
            JLabel picLabel = new JLabel(new ImageIcon(pic));
            cells[curInd].add(picLabel);
            picLabel.setBounds(0, 0, cells[curInd].cellSize, cells[curInd].cellSize);
        } else {
            cells[curInd].remove(0);
            cells[curInd].repaint();
            JLabel picLabel = new JLabel(new ImageIcon(pic));
            cells[curInd].add(picLabel);
            picLabel.setBounds(0, 0, cells[curInd].cellSize, cells[curInd].cellSize);
        }
    }

    ;
//TODO проверка проигрыша противника
    void CreateOppBug(int oppInd) {
        if (cells[oppInd].getCurrentStat().equals(Cell.status.EMPTY)) {
            if (myBaseIndex == cellsCntW + 1) {
                addPicToCell(oppInd, rBugPic);
            }
            if (myBaseIndex == cellsCntW * (cellsCntH - 1) - 2) {
                addPicToCell(oppInd, bBugPic);
            }
            cells[oppInd].setCurrentStat(Cell.status.OPP_BUG);
            movesCnt++;
            if (movesCnt == 3) {
                changeCurPlayerIndex();
            }
        }

        if (cells[oppInd].getCurrentStat().equals(Cell.status.MY_BUG)) {
            if (myBaseIndex == cellsCntW + 1) {
                addPicToCell(oppInd, kbBugPic);
            }
            if (myBaseIndex == cellsCntW * (cellsCntH - 1) - 2) {
                addPicToCell(oppInd, krBugPic);
            }
            cells[oppInd].setCurrentStat(Cell.status.KILLED_BY_OPP);
            movesCnt++;
            if (movesCnt == 3) {
                changeCurPlayerIndex();
            }
        }
        if (getAvailableCells(myBaseIndex).isEmpty()) {
            JOptionPane.showMessageDialog(this, "You loose...");
        }
        if (getAvailableCells(oppBaseIndex).isEmpty()) {
            JOptionPane.showMessageDialog(this, "You win!!!");
        }

    }

    ;

    void changeCurPlayerIndex() {
        if (curPlayerIndex == 1) {
            curPlayerIndex = 2;
            return;
        }
        if (curPlayerIndex == 2) {
            curPlayerIndex = 1;
            return;
        }
    }

    void CreateBug(java.awt.event.MouseEvent evt, int curInd, int baseIndex) {
        if (Player.getPlayerNumber() == curPlayerIndex) {
            if (movesCnt != 0) {
                if (!cells[curInd].getCurrentStat().equals(Cell.status.MY_BASE)
                        && !cells[curInd].getCurrentStat().equals(Cell.status.OPP_BASE)
                        && !cells[curInd].getCurrentStat().equals(Cell.status.MY_BUG)
                        && !cells[curInd].getCurrentStat().equals(Cell.status.KILLED_BY_OPP)
                        && !cells[curInd].getCurrentStat().equals(Cell.status.KILLED_BY_ME)) {
                    if (CheckArea(curInd, Cell.status.MY_BUG) || CheckArea(curInd, Cell.status.MY_BASE)
                            || CheckArea(curInd, Cell.status.KILLED_BY_ME)) {
                        if (getAvailableCells(myBaseIndex).contains(cells[curInd])) {
                            if (cells[curInd].getCurrentStat().equals(Cell.status.EMPTY)) {
                                if (baseIndex == cellsCntW + 1) {
                                    addPicToCell(curInd, bBugPic);
                                }
                                if (baseIndex == cellsCntW * (cellsCntH - 1) - 2) {
                                    addPicToCell(curInd, rBugPic);
                                }
                                cells[curInd].setCurrentStat(Cell.status.MY_BUG);
                                try {
                                    Player.getOutputStream().writeUTF(Integer.toString(curInd));
                                    Player.getOutputStream().flush();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                movesCnt--;
                                if (movesCnt == 0) {
                                    changeCurPlayerIndex();
                                }

                            }

                            if (cells[curInd].getCurrentStat().equals(Cell.status.OPP_BUG)) {
                                if (baseIndex == cellsCntW + 1) {
                                    addPicToCell(curInd, krBugPic);
                                }
                                if (baseIndex == cellsCntW * (cellsCntH - 1) - 2) {
                                    addPicToCell(curInd, kbBugPic);
                                }

                                cells[curInd].setCurrentStat(Cell.status.KILLED_BY_ME);
                                try {
                                    Player.getOutputStream().writeUTF(Integer.toString(curInd));
                                    Player.getOutputStream().flush();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                movesCnt--;
                                if (movesCnt == 0) {
                                    changeCurPlayerIndex();
                                }
                            }
                        }
                    }

                }
            }
        }
        if (getAvailableCells(myBaseIndex).isEmpty()) {
            JOptionPane.showMessageDialog(this, "You loose...");
        }
        if (getAvailableCells(oppBaseIndex).isEmpty()) {
            JOptionPane.showMessageDialog(this, "You win!!!");
        }
    }

    ;

    void addToList(int curInd, Cell.status checkedStatus, List<Cell> availableCells) {
        if (cells[curInd].getCurrentStat().equals(checkedStatus)) {
            if (!availableCells.contains(cells[curInd])) {
                availableCells.add(cells[curInd]);
            }
        }
    }

    void addCheckedCellsToList(int curInd, Cell.status checkedStatus, List<Cell> availableCells) {
        //если  жука в первой строке
        if (curInd < cellsCntW) {
            //если в первой клетке
            if (curInd == 0) {
                addToList(curInd + 1, checkedStatus, availableCells);
                addToList(curInd + cellsCntW, checkedStatus, availableCells);
                addToList(curInd + cellsCntW + 1, checkedStatus, availableCells);
                return;
            }
            //если в последней клетке
            if (curInd == cellsCntW - 1) {
                addToList(curInd - 1, checkedStatus, availableCells);
                addToList(curInd + cellsCntW, checkedStatus, availableCells);
                addToList(curInd + cellsCntW - 1, checkedStatus, availableCells);
                return;
            }
            //остальные клетки первой строки

            addToList(curInd + 1, checkedStatus, availableCells);
            addToList(curInd - 1, checkedStatus, availableCells);
            addToList(curInd + cellsCntW - 1, checkedStatus, availableCells);
            addToList(curInd + cellsCntW, checkedStatus, availableCells);
            addToList(curInd + cellsCntW + 1, checkedStatus, availableCells);
            return;

        }

        //если хочешь поставить жука в последнюю строку
        if (curInd > (cellsCntW * (cellsCntH - 1) - 1)) {
            //если в первую клетку последней строки
            if (curInd == (cellsCntW - 1) * cellsCntH) {
                addToList(curInd + 1, checkedStatus, availableCells);
                addToList(curInd - cellsCntW, checkedStatus, availableCells);
                addToList(curInd - cellsCntW + 1, checkedStatus, availableCells);
                return;
            }
            //если в последнюю клетку последней строки
            if (curInd == cellsCntW * cellsCntH - 1) {
                addToList(curInd - 1, checkedStatus, availableCells);
                addToList(curInd - cellsCntW, checkedStatus, availableCells);
                addToList(curInd - cellsCntW - 1, checkedStatus, availableCells);
                return;
            }
            //остальные клетки последней строки
            addToList(curInd + 1, checkedStatus, availableCells);
            addToList(curInd - 1, checkedStatus, availableCells);
            addToList(curInd - cellsCntW - 1, checkedStatus, availableCells);
            addToList(curInd - cellsCntW, checkedStatus, availableCells);
            addToList(curInd - cellsCntW + 1, checkedStatus, availableCells);
            return;
        }
        //если хочешь поставить клетку в крайний левый столбец
        if (curInd % cellsCntW == 0) {
            addToList(curInd + 1, checkedStatus, availableCells);
            addToList(curInd - cellsCntW, checkedStatus, availableCells);
            addToList(curInd - cellsCntW + 1, checkedStatus, availableCells);
            addToList(curInd + cellsCntW, checkedStatus, availableCells);
            addToList(curInd + cellsCntW + 1, checkedStatus, availableCells);
            return;
        }
        // -//- крайний правый
        if (curInd % cellsCntW == cellsCntW - 1) {
            addToList(curInd - 1, checkedStatus, availableCells);
            addToList(curInd - cellsCntW, checkedStatus, availableCells);
            addToList(curInd - cellsCntW - 1, checkedStatus, availableCells);
            addToList(curInd + cellsCntW, checkedStatus, availableCells);
            addToList(curInd + cellsCntW - 1, checkedStatus, availableCells);
            return;
        }
        // в середину поля
        addToList(curInd - 1, checkedStatus, availableCells);
        addToList(curInd + 1, checkedStatus, availableCells);
        addToList(curInd - cellsCntW, checkedStatus, availableCells);
        addToList(curInd - cellsCntW - 1, checkedStatus, availableCells);
        addToList(curInd - cellsCntW + 1, checkedStatus, availableCells);
        addToList(curInd + cellsCntW - 1, checkedStatus, availableCells);
        addToList(curInd + cellsCntW, checkedStatus, availableCells);
        addToList(curInd + cellsCntW + 1, checkedStatus, availableCells);
        return;
    }

    ;

    List<Cell> getAvailableCells(int baseIndex) {
        List<Cell> availableCells = new ArrayList<Cell>();
        List<Cell> uncheckedBugs = new ArrayList<Cell>();
        uncheckedBugs.add(cells[baseIndex]);
        int curIndex = 0;
        if (baseIndex == myBaseIndex) {
            do {
                addCheckedCellsToList(uncheckedBugs.get(curIndex).getIndex(), Cell.status.EMPTY, availableCells);
                addCheckedCellsToList(uncheckedBugs.get(curIndex).getIndex(), Cell.status.OPP_BUG, availableCells);
                addCheckedCellsToList(uncheckedBugs.get(curIndex).getIndex(), Cell.status.MY_BUG, uncheckedBugs);
                addCheckedCellsToList(uncheckedBugs.get(curIndex).getIndex(), Cell.status.KILLED_BY_ME, uncheckedBugs);
                curIndex++;
            } while (curIndex < uncheckedBugs.size());
        }

        if (baseIndex == oppBaseIndex) {
            do {
                addCheckedCellsToList(uncheckedBugs.get(curIndex).getIndex(), Cell.status.EMPTY, availableCells);
                addCheckedCellsToList(uncheckedBugs.get(curIndex).getIndex(), Cell.status.MY_BUG, availableCells);
                addCheckedCellsToList(uncheckedBugs.get(curIndex).getIndex(), Cell.status.OPP_BUG, uncheckedBugs);
                addCheckedCellsToList(uncheckedBugs.get(curIndex).getIndex(), Cell.status.KILLED_BY_OPP, uncheckedBugs);
                curIndex++;
            } while (curIndex < uncheckedBugs.size());
        }

        return availableCells;
    }
;

}
