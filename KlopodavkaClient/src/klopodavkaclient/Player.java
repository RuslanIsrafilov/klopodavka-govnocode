package klopodavkaclient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Player {

    private static Socket socket;
    private static DataInputStream inputStream;
    private static DataOutputStream outputStream;
    private static String name;
    private static int playerNumber;

    public static int getPlayerNumber() {
        return playerNumber;
    }

    public static void setPlayerNumber(int number) {
        playerNumber = number;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String newName) {
        name = newName;
    }

    public static Socket getSocket() {
        return socket;
    }

    public static void setSocket(Socket new_socket) {
        socket = new_socket;
    }

    public static DataInputStream getInputStream() throws IOException {
        if (inputStream == null) {
            inputStream = new DataInputStream(socket.getInputStream());
        }
        return inputStream;

    }

    public static DataOutputStream getOutputStream() throws IOException {
        if (outputStream == null) {
            outputStream = new DataOutputStream(socket.getOutputStream());
        }
        return outputStream;
    }
}
