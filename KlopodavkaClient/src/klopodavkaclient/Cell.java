package klopodavkaclient;

import javax.swing.*;
import java.awt.*;

public class Cell extends JPanel {

    enum status {

        EMPTY,
        MY_BUG,
        OPP_BUG,
        KILLED_BY_ME,
        KILLED_BY_OPP,
        MY_BASE,
        OPP_BASE
    };

    int index;
    int cellSize;
    status currentStat;

    Cell(int newIndex, int newCellSize, status newStat) {
        index = newIndex;
        cellSize = newCellSize;
        currentStat = newStat;
    }

    int getIndex() {
        return index;
    }

    ;

    void setIndex(int newIndex) {
        index = newIndex;
    }

    ;

    status getCurrentStat() {
        return currentStat;
    }

    ;

    void setCurrentStat(status newStat) {
        currentStat = newStat;
    }
;

}
