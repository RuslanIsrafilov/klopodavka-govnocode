package klopodavkaserver;

import klopodavkaserver.PlayerOnServer;
import java.io.IOException;

public class Game implements Runnable {

    private PlayerOnServer playerOne;
    private PlayerOnServer playerTwo;
    private int turnLength;

    public Game(PlayerOnServer playerOne, PlayerOnServer playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        turnLength = 3;
    }

    @Override
    public void run() {
        try {
            playerOne.getOutputStream().writeUTF("1");
            playerOne.getOutputStream().flush();

            playerTwo.getOutputStream().writeUTF("2");
            playerTwo.getOutputStream().flush();

            playerTwo.getOutputStream().writeUTF(playerOne.getInputStream().readUTF());
            playerTwo.getOutputStream().flush();

            playerOne.getOutputStream().writeUTF(playerTwo.getInputStream().readUTF());
            playerOne.getOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (true) {
            try {
                for (int i = 0; i < turnLength; i++) {
                    playerTwo.getOutputStream().writeUTF(playerOne.getInputStream().readUTF());
                    playerTwo.getOutputStream().flush();
                }

                for (int i = 0; i < turnLength; i++) {
                    playerOne.getOutputStream().writeUTF(playerTwo.getInputStream().readUTF());
                    playerOne.getOutputStream().flush();
                }
            } catch (IOException e) {
                return;
            }
        }
    }
}
