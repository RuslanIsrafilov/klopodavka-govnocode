package klopodavkaserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Scanner;

public class Server {

    private static ServerSocket serverSocket;
    private static final int port = 3124;
    public static boolean isStopped;

    public static void main(String[] args) {
        System.out.println("Server is launching...\nType 'exit' to close application\n");
        initializeSocket();
        System.out.println("Waiting for players...\n");
        waitingForPlayers();
        checkForExit();
    }

    private static void initializeSocket() {
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("Couldn't listen to port " + port);
            System.exit(-1);
        }
    }

    private static void waitingForPlayers() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    PlayerOnServer playerOne = new PlayerOnServer(), playerTwo = new PlayerOnServer();
                    try {
                        System.out.println("1st player waiting...");
                        playerOne.setSocket(serverSocket.accept());
                        System.out.println("2nd player waiting...");
                        playerTwo.setSocket(serverSocket.accept());
                    } catch (IOException e) {
                        if (isStopped) {
                            return;
                        }
                        e.printStackTrace();
                    }

                    startGame(playerOne, playerTwo);
                }
            }
        }).start();
    }

    private static void checkForExit() {
        String forExit = "";
        Scanner scanner = new Scanner(System.in);
        while (!forExit.equalsIgnoreCase("exit")) {
            forExit = scanner.nextLine();
        }
        isStopped = true;
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }

    private static void startGame(PlayerOnServer playerOne, PlayerOnServer playerTwo) {
        new Thread(new Game(playerOne, playerTwo)).start();
    }
}
