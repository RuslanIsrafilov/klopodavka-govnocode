package klopodavkaserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class PlayerOnServer {

    private Socket socket;
    private DataInputStream inputStream;
    private DataOutputStream outputStream;

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public DataInputStream getInputStream() throws IOException {
        if (inputStream == null) {
            inputStream = new DataInputStream(socket.getInputStream());
        }
        return inputStream;

    }

    public DataOutputStream getOutputStream() throws IOException {
        if (outputStream == null) {
            outputStream = new DataOutputStream(socket.getOutputStream());
        }
        return outputStream;
    }
}
